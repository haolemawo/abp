﻿/// <summary>
/// 广告类型
/// </summary>
public enum AdType
{
    Image,
    Text,
    Html,
    JavsScript,
    JavsScriptLink
}