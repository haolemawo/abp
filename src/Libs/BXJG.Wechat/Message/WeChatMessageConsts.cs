﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.WeChat.Message
{
    public class WeChatMessageConsts
    {
        /// <summary>
        /// 微信订阅消息发送请求地址
        /// </summary>
        public const string WechatSendMessageUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send";

    }
}
