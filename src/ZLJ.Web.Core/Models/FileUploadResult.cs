﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZLJ.Models
{
    /// <summary>
    /// 上传文件的响应数据
    /// </summary>
    public class FileUploadResult
    {
        /// <summary>
        /// 相对路径
        /// </summary>
        public string RelativePath { get; set; }
    }
}
